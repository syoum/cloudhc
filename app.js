var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var multer = require('multer');

var hc = express()

// Initialise middlewares

hc.use(cookieParser());
hc.use(bodyParser.json());
hc.use(bodyParser.urlencoded({
	extended: true
}));

// Database setup

require('./src/models/db.js');

// Serve static content under root

hc.use('/', express.static('public'));

// Capture API calls

var routes = require('./src/routes/routes.js');
hc.use('/api', routes);

// Serve static content

hc.use(express.static('public'));

// Capture invalid calls

hc.all('*', (req, res) => {
	res.sendStatus(403);
})

// Start

var PORT = process.env.PORT || 3000;

hc.listen(PORT, () => {
	console.log('CloudHC is up!');
})