var mongoose = require('mongoose');
var uuid = require('uuid/v1');
var USER = mongoose.model('USER');

var createUser = (req, res) => {
	USER.findOne({email: req.body.email}, (err, doc) => {
		if (doc) {
			res.status(403).send();
		} else {
			sid = uuid();
			var newUser = new USER({
				name: req.body.name,
				home: req.body.home,
				phone: req.body.phone,
				email: req.body.email,
				password: req.body.password,
				session: sid
			})
			newUser.save((err, doc) => {
				if (!err) {
					res.cookie("session", sid)
					res.status(200).send();
				} else {
					res.status(500).send();
				}
			})
		}
	})
}

var login = (req, res) => {
	USER.findOne({email: req.body.email}, (err, doc) => {
		if (doc) {
			if (req.body.password == doc.password) {
				sid = uuid();
				doc.session = sid;
				doc.save()
					.then(() => {
						res.cookie('session', sid);
						if (doc.admin) {
							res.cookie('admin', true);
						}
						res.status(200).send()})
				
			}
			else {
				res.status(403).send();
			}
		} else {
			res.status(404).send();
		}
	})
}

var getUserInfo = (req, res) => {
	if (!req.cookies.session) {
		res.status(401).send();
	} else {
		USER.findOne({session: req.cookies.session}, (err, doc) => {
			if (!doc) {
				res.status(404).send();
			} else {
				res.send(stripContent(doc));
			}
		})
	}
}

var updateUserInfo = (req, res) => {
	if (!req.cookies.session) {
		res.status(401).send();
	} else {
		USER.findOneAndUpdate({session: req.cookies.session}, req.body, (err, doc) => {
			if (err) {
				res.status(500).send();
			} else {
				res.status(200).send();
			}
		})
	}
}

var stripContent = (doc) => {
	striped = {}
	striped.name = doc.name;
	striped.home = doc.home;
	striped.phone = doc.phone;
	striped.email = doc.email;
	return striped;
}

module.exports.createUser = createUser;
module.exports.login = login;
module.exports.getUserInfo = getUserInfo;
module.exports.updateUserInfo = updateUserInfo;