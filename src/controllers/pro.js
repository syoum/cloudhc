var mongoose = require('mongoose');
var PRO = mongoose.model('PRO');

var getAllPros = (req, res) => {
	PRO.find({}, (err, docs) => {
		res.send(docs);
	})
}

var createPro = (req, res) => {

	PRO.findOne({email: req.body.email}, (err, docs) => {
		if (docs) {
			res.status(403).send();
		} else {

			var newPro = new PRO({
				type: req.body.type,
				name: req.body.name,
				email: req.body.email,
				rate: req.body.rate
			})

			newPro.save((err, newPro) => {
				if (!err) {
					res.send(newPro);
				} else {
					res.status(500).send();
				}
			})
		}
	})
}

module.exports.getAllPros = getAllPros;
module.exports.createPro = createPro;