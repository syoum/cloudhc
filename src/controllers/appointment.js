var mongoose = require('mongoose');
var APPOINTMENT = mongoose.model('APPOINTMENT');
var USER = mongoose.model('USER');
var PRO = mongoose.model('PRO');
var sgMail = require("@sendgrid/mail");
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

var getAllAppointments = (req, res) => {
	if (req.cookies.admin) {
		APPOINTMENT.find((err, docs) => {
			res.send(docs);
		})
	} else {
		USER.findOne({session: req.cookies.session}, (err, doc) => {
			if (!doc) {
				res.status(401).send();
			} else {
				APPOINTMENT.find({user: doc._id}, (err, docs) => {
					if (!err) {
						res.send(docs);
					} else {
						res.status(500).send();
					}
				})
			}
		})
	}
}

var createAppointment = (req, res) => {

	USER.findOne({session: req.cookies.session}, (err, doc) => {
		if (!err) {
			var newApp = APPOINTMENT({
				pro: req.body.pro,
				user: doc._id,
				from: req.body.from,
				duration: req.body.duration,
				msg: req.body.msg
			});

			newApp.save((err, doc) => {
				if (!err) {
					PRO.findById(req.body.pro, "email name", (err, pro) => {
						sendEmail(true, pro.email, pro.name, req.body.from, req.body.duration, req.body.msg);
					})
					res.send();
				} else {
					res.status(500).send();
				}
			})
		} else {
			res.status(401).send();				
		}
	});
}

var cancelAppointment = (req, res) => {
	USER.findOne({session: req.cookies.session}, (err, doc) => {
		if (!err) {
			
			APPOINTMENT.findByIdAndDelete(req.params.id, (err, doc) => {
				if (!err) {
					PRO.findById(doc.pro, "email name", (err, pro) => {
						sendEmail(false, pro.email, pro.name, doc.from, 0, "");
					})
					res.send();
				} else {
					res.status(404).send();
				}
			});

		} else {
			res.status(401).send();				
		}
	});
}

var sendEmail = (status, email, name, time, duration, msg) => {
	var ml = {
		to: email,
		from: "cloudhc@mail.stvia.me",
		subject: status ? "[CloudHC] NEW APPOINTMENT" : "[CloudHC] AN APPOINTMENT CANCELLED",
	}
	if (status) {
		ml.text = "Hello " + name + ", you have a new appointment at " + new Date(time).toLocaleString(("en-AU"), {timeZone: "Australia/Melbourne"}) + " for " + duration + "hr(s). Patient left a message: " + msg
	} else {
		ml.text = "Hello " + name + ", your appointment at " + new Date(time).toLocaleString(("en-AU"), {timeZone: "Australia/Melbourne"}) + " is cancelled. "
	}

	sgMail.send(ml);
}

module.exports.getAllAppointments = getAllAppointments;
module.exports.createAppointment = createAppointment;
module.exports.cancelAppointment = cancelAppointment;