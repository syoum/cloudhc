var mongoose = require('mongoose');

var proSchema = mongoose.Schema({
	type: String,
	name: String,
	email: String,
	rate: Number
});

var userSchema = mongoose.Schema({
	name: String,
	home: String,
	phone: String,
	email: String,
	password: String,
	session: String,
	admin: Boolean
});

var appointmentSchema = mongoose.Schema({
	pro: String,
	user: String,
	from: Number,
	duration: Number,
	msg: String
})

mongoose.model('PRO', proSchema);
mongoose.model('USER', userSchema);
mongoose.model('APPOINTMENT', appointmentSchema);