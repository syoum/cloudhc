var express = require('express');
var pro = express.Router();

var con = require('../controllers/pro.js');

// Get all pros

pro.get('/', con.getAllPros);

// Create a pro

pro.post('/', con.createPro);

module.exports = pro