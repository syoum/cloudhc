var express = require('express');
var routes = express.Router();

var pro = require('./pro.js');
var user = require('./user.js');
var appointment = require('./appointment.js');

routes.use('/pro', pro);
routes.use('/user', user);
routes.use('/appointment', appointment);

module.exports = routes;