var express = require('express');
var appointment = express.Router();

var con = require('../controllers/appointment.js');

appointment.get('/', con.getAllAppointments);
appointment.post('/', con.createAppointment);
appointment.delete('/:id', con.cancelAppointment);

module.exports = appointment;