var express = require('express');
var user = express.Router();

var con = require('../controllers/user.js');

// Create user

user.post('/', con.createUser);

// Get user info

user.get('/', con.getUserInfo);

// Update user info

user.put('/', con.updateUserInfo);

// Login

user.post('/login', con.login);

module.exports = user;