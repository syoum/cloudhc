// Libraries

const HCS = window.location.href + "api/";

const HCAPI = {
	pro: HCS + "pro/",
	user: HCS + "user/",
	userLogin: HCS + "user/login/",
	appointment: HCS + "appointment/"
}

const eid = (id) => {
	return document.getElementById(id);
}

const ec = (c) => {
	return document.getElementsByClassName(c);
}

const navActivate = (e) => {

	for (var i = 0; i < app.navItems.length; i++) {
		app.navItems[i].classList.remove("nav-item-active");
	}

	e.target.classList.add("nav-item-active");

	switch (e.target.id) {
		case "navProfessional": {
			frameActivate("frameProfessional");
			break;
		}
		case "navAppointment": {

			if (!cookies.session) {
				app.navItems[3].click();
				break;
			}

			frameActivate("frameAppointment");
			break;
		}
		case "navUser": {
			frameActivate("frameUser");
			break;
		}
		default: {
			frameActivate("frameHero");
		}
	}
}

const frameActivate = (f) => {

	for (var i = 0; i < app.frames.length; i++) {
		app.frames[i].classList.remove("frame-active");

		if (f == app.frames[i].id) {
			app.frames[i].classList.add("frame-active");
		}
	}
}

const doLogin = () => {

	if (app.loginEmail.value.indexOf("@") == -1) {
		app.loginNote.innerText = "Please enter the correct email address.";
		app.loginNote.classList.remove("hidden");
		return;
	}

	if (app.loginEmail.value == "" || app.loginPassword.value == "") {
		app.loginNote.innerText = "Please complete the fields. ";
		app.loginNote.classList.remove("hidden");
		return;
	}

	var xhr = new XMLHttpRequest();

	var payload = {
		email: app.loginEmail.value,
		password: app.loginPassword.value
	};

	xhr.onreadystatechange = () => {
		if (xhr.readyState == 4) {
			if (xhr.status == 404) {
				app.loginNote.innerText = "Sorry, account does not exist. ";
				app.loginNote.classList.remove("hidden");
			}
			if (xhr.status == 403) {
				app.loginNote.innerText = "Sorry, wrong password. ";
				app.loginNote.classList.remove("hidden");
			}
			if (xhr.status == 200) {
				app.loginNote.classList.add("hidden");
				app.userLogin.classList.add("hidden");
				app.userProfile.classList.remove("hidden");
				getUserProfile();
				getAllApps();
			}
		}
	}

	xhr.open("POST", HCAPI.userLogin + "?ts=" + new Date().getTime(), true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(payload));
}

const doSignup = () => {

	app.loginName.classList.remove("hidden");
	app.loginPhone.classList.remove("hidden");
	app.loginAddress.classList.remove("hidden");

	if (app.loginEmail.value.indexOf("@") == -1) {
		app.loginNote.innerText = "Please enter the correct email address.";
		app.loginNote.classList.remove("hidden");
		return;
	}

	if (app.loginEmail.value == "" || app.loginPassword.value == "" || app.loginName.value == "" || app.loginPhone.value == "" || app.loginAddress.value == "") {
		app.loginNote.innerText = "Please complete the fields. ";
		app.loginNote.classList.remove("hidden");
		return;
	}

	var xhr = new XMLHttpRequest();

	var payload = {
		email: app.loginEmail.value,
		password: app.loginPassword.value,
		name: app.loginName.value,
		phone: app.loginPhone.value,
		home: app.loginAddress.value
	}

	xhr.onreadystatechange = () => {
		if (xhr.readyState == 4) {
			if (xhr.status == 403) {
				app.loginNote.innerText = "Sorry, account already exists. ";
				app.loginNote.classList.remove("hidden");
			}
			if (xhr.status == 500) {
				app.loginNote.innerText = "Sorry, server returned an error. ";
				app.loginNote.classList.remove("hidden");
			}
			if (xhr.status == 200) {
				app.loginName.classList.add("hidden");
				app.loginPhone.classList.add("hidden");
				app.loginAddress.classList.add("hidden");
				app.userLogin.classList.add("hidden");
				app.userProfile.classList.remove("hidden");
				app.loginNote.classList.add("hidden");
				getUserProfile();
			}
		}
	}

	xhr.open("POST", HCAPI.user + "?ts=" + new Date().getTime(), true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(payload));
}

const doUpdateInfo = () => {

	if (app.userInfoEditBtn.innerText == "EDIT") {
		app.userInfoEditBtn.innerText = "UPDATE";
		app.userName.disabled = false;
		app.userAddress.disabled = false;
		app.userPhone.disabled = false;
		return;
	}

	if (app.userName.value == "" || app.userAddress.value == "" || app.userAddress.value == "") {
		app.profileNote.innerText = "Please complete the fields. ";
		app.profileNote.classList.remove("hidden");
		return;
	}

	app.userInfoEditBtn.innerText = "EDIT";
	app.profileNote.classList.add("hidden");

	var xhr = new XMLHttpRequest();

	var payload = {
		name: app.userName.value,
		phone: app.userPhone.value,
		address: app.userAddress.value
	}

	xhr.onreadystatechange = () => {
		if (xhr.readyState == 4) {

			if (xhr.status == 200) {
				getUserProfile();
			}
		}
	}

	xhr.open("PUT", HCAPI.user + "?ts=" + new Date().getTime(), true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(payload));
}

const getUserProfile = () => {

	updateCookies();

	if (!cookies.session) {
		return;
	}

	app.userLogin.classList.add("hidden");
	app.userProfile.classList.remove("hidden");

	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = () => {
		if (xhr.readyState == 4) {
			if (xhr.status == 404) {
				document.cookie = "session=; expires=Thu, 01 Jan 1970 00:00:01 GMT";
				document.cookie = "admin=; expires=Thu, 01 Jan 1970 00:00:01 GMT";
				app.userLogin.classList.remove("hidden");
				app.userProfile.classList.add("hidden");
			}
			if (xhr.status == 200) {
				var payload = xhr.response;
				app.userEmail.innerText = payload.email;
				app.userName.value = payload.name;
				app.userPhone.value = payload.phone;
				app.userAddress.value = payload.home;
				app.userName.disabled = true;
				app.userPhone.disabled = true;
				app.userAddress.disabled = true;

				if (cookies.admin == "true") {
					app.newProBtn.classList.remove("hidden");
				} else {
					app.newAppBtn.classList.remove("hidden");
				}
			}
		}
	}

	xhr.responseType = "json";
	xhr.open("GET", HCAPI.user + "?ts=" + new Date().getTime(), true);
	xhr.send();
}

const updateCookies = () => {
	cookies = {};
	document.cookie.split(" ").join("").split(";").forEach((cookie) => {
		cookies[cookie.split("=")[0]] = cookie.split("=")[1];
	});
}

const doLogout = () => {
	document.cookie = "session=; expires=Thu, 01 Jan 1970 00:00:01 GMT";
	document.cookie = "admin=; expires=Thu, 01 Jan 1970 00:00:01 GMT";

	updateCookies();

	app.userLogin.classList.remove("hidden");
	app.userProfile.classList.add("hidden");
	app.userEmail.innerText = "";
	app.userName.value = "";
	app.userPhone.value = "";
	app.userAddress.value = "";
	app.newAppBtn.classList.add("hidden");
	app.newProBtn.classList.add("hidden");
	app.newAppPanel.classList.add("hidden");
	app.newProPanel.classList.add("hidden");

	var j = app.appList.children.length;
	for (var i = 0; i < j; i++) {
		app.appList.removeChild(app.appList.children[0]);
	}

}

const getAllPros = () => {

	var j = app.proList.children.length;
	for (var i = 0; i < j; i++) {
		app.proList.removeChild(app.proList.children[0]);
	}

	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = () => {
		if (xhr.readyState == 4) {
			if (xhr.status == 200) {
				pros = xhr.response;

				addProToHero();

				while (app.newAppPro.children.length > 0) {
					app.newAppPro.remove(0);
				}

				pros.forEach((pro) => {
					addProToList(pro.name, pro.type, pro.rate);
					var opt = document.createElement("option");
					opt.value = pro._id;
					opt.innerText = "[" + pro.type + "] " + pro.name;
					app.newAppPro.add(opt);
				})
			}
		}
	}

	xhr.responseType = "json";
	xhr.open("GET", HCAPI.pro + "?ts=" + new Date().getTime(), true);
	xhr.send();
}

const addProToHero = () => {

	while (app.heroProList.children.length > 0) {
		app.heroProList.removeChild(app.heroProList.children[0]);
	}

	for (var i = 0; i < Math.min(5, pros.length); i++) {
		var heroProPanel = document.createElement("div");
		heroProPanel.classList.add("hero-pro-panel");
		var heroProAvatar = document.createElement("div");
		var heroProName = document.createElement("div");
		var heroProType = document.createElement("div");
		heroProAvatar.classList.add("hero-pro-avatar");
		heroProName.classList.add("hero-pro-name");
		heroProType.classList.add("hero-pro-type");
		heroProName.innerText = pros[i].name;
		heroProType.innerText = pros[i].type;
		heroProPanel.appendChild(heroProAvatar);
		heroProPanel.appendChild(heroProName);
		heroProPanel.appendChild(heroProType);
		app.heroProList.appendChild(heroProPanel);
	}
}

const addProToList = (name, type, rate) => {
	var proItem = document.createElement("div");
	proItem.classList.add("pro-item");
	var proName = document.createElement("span");
	proName.classList.add("pro-name");
	proName.innerText = name;
	proItem.appendChild(proName);
	var proType = document.createElement("span");
	proType.classList.add("pro-type");
	proType.innerText = type;
	proItem.appendChild(proType);
	var proRate = document.createElement("span");
	proRate.classList.add("pro-rate");
	proRate.innerText = "$ " + rate;
	proItem.appendChild(proRate);
	app.proList.appendChild(proItem);
}

const getAllApps = () => {

	var j = app.appList.children.length;
	for (var i = 0; i < j; i++) {
		app.appList.removeChild(app.appList.children[0]);
	}

	if (!cookies.session) {
		return;
	}

	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = () => {
		if (xhr.readyState == 4) {
			if (xhr.status == 200) {
				apps = xhr.response;
				apps.forEach((app) => {
					var pro = "";
					pros.forEach((_pro) => {
						if (_pro._id == app.pro) {
							pro = _pro.name;
						}
					});
					addAppToList(app._id, app.from, app.duration, pro);
				})
			}
		}
	}

	xhr.responseType = "json";
	xhr.open("GET", HCAPI.appointment + "?ts=" + new Date().getTime(), true);
	xhr.send();
}

const addAppToList = (id, dateTime, duration, pro) => {
	var appItem = document.createElement("div");
	var appCancel = document.createElement("span");
	appCancel.classList.add("app-cancel");
	appCancel.innerText = "DELETE";
	appCancel.setAttribute("id", id);
	appCancel.addEventListener("click", tryDelete);
	appCancel.addEventListener("mouseout", restoreDelete);
	appItem.appendChild(appCancel);
	appItem.classList.add("app-item");
	var appDateTime = document.createElement("span");
	appDateTime.classList.add("app-dateTime");
	var dateTime = new Date(dateTime);
	dateTime = dateTime.toString();
	dateTime = dateTime.split(" ").splice(0, 5).join(" ");
	appDateTime.innerText = dateTime;
	appItem.appendChild(appDateTime);
	var appDuration = document.createElement("span");
	appDuration.classList.add("app-duration");
	appDuration.innerText = duration + " hr(s)";
	appItem.appendChild(appDuration);
	var appPro = document.createElement("span");
	appPro.classList.add("app-pro");
	appPro.innerText = pro
	appItem.appendChild(appPro);
	app.appList.appendChild(appItem);
}

const addNewPro = () => {

	if (app.newProName.value == "" || app.newProRate.value == "" || app.newProEmail.value == "") {
		app.newProNote.classList.remove("hidden");
		app.newProNote.innerText = "Please complete the fields. ";
		return;
	}

	if (app.newProEmail.value.indexOf("@") == -1) {
		app.newProNote.classList.remove("hidden");
		app.newProNote.innerText = "Incorrect email. ";
		return;
	}

	app.newProNote.classList.add("hidden");

	var payload = {
		name: app.newProName.value,
		rate: app.newProRate.value,
		email: app.newProEmail.value,
		type: app.newProType.value
	}

	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = () => {
		if (xhr.readyState == 4) {
			if (xhr.status == 403) {
				app.newProNote.classList.remove("hidden");
				app.newProNote.innerText = "Professional already exists. ";
			}
			if (xhr.status == 500) {
				app.newProNote.classList.remove("hidden");
				app.newProNote.innerText = "Server returned an error. ";
			}
			if (xhr.status == 200) {
				app.newProNote.classList.add("hidden");
				app.newProPanel.classList.add("hidden");
				app.newProName.value = "";
				app.newProEmail.value = "";
				app.newProRate.value = "";
				getAllPros();
			}
		}
	}

	xhr.open("POST", HCAPI.pro, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(payload));
}

const addNewApp = () => {

	if (app.newAppY.value == "y" || app.newAppM.value == "m" || app.newAppD.value == "d" || app.newAppH.value == "h") {
		app.newAppNote.classList.remove("hidden");
		app.newAppNote.innerText = "Please select consultation time. ";
		return;
	}

	app.newAppNote.classList.add("hidden");

	var payload = {
		pro: app.newAppPro.value,
		duration: app.newAppDuration.value,
		msg: app.newAppMsg.value,
		from: new Date(app.newAppY.value, app.newAppM.value - 1, app.newAppD.value, app.newAppH.value).getTime()
	}

	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = () => {
		if (xhr.readyState == 4) {
			if (xhr.status == 200) {
				app.newAppPanel.classList.add("hidden");
				app.newAppNote.value = "";
				getAllApps();
			}
		}
	}

	xhr.open("POST", HCAPI.appointment, true);
	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(payload));
}

const YChanged = () => {
	changeM(app.newAppY.value);
}

const MChanged = () => {
	changeD(32 - new Date(app.newAppY.value, app.newAppM.value - 1, 32).getDate());
}

const DChanged = () => {
	changeH();
}

const changeM = (y) => {

	while (app.newAppM.children.length > 2) {
		app.newAppM.remove(2);
	}

	if (y == "y") {
		return;
	}

	for (var i = 1; i <= 12; i++) {
		var opt = document.createElement("option");
		opt.value = i;
		opt.innerText = i;
		app.newAppM.add(opt);
	}
}

const changeD = (d) => {

	while (app.newAppD.children.length > 2) {
		app.newAppD.remove(2);
	}

	for (var i = 1; i <= d; i++) {
		var opt = document.createElement("option");
		opt.value = i;
		opt.innerText = i;
		app.newAppD.add(opt);
	}
}

const changeH = () => {

	while (app.newAppH.children.length > 2) {
		app.newAppH.remove(2);
	}

	var dateString = new Date(app.newAppY.value, app.newAppM.value - 1, app.newAppD.value - 1).toString();
	app.newAppH.children[0].innerText = "HH";

	if (dateString.indexOf("Sat") != -1 || dateString.indexOf("Sun") != -1) {
		app.newAppH.children[0].innerText = "Weekend";
		return;
	}

	for (var i = 9; i < 17; i++) {
		var opt = document.createElement("option");
		opt.innerText = i + ":00";
		opt.value = i;
		app.newAppH.add(opt);
	}
}

const tryDelete = (e) => {

	if (e.target.innerText == "DELETE") {
		e.target.innerText = "ARE YOU SURE?";
		return;
	}

	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = () => {
		if (xhr.readyState == 4) {
			if (xhr.status == 200) {
				getAllApps();
			}
		}
	}

	xhr.open("DELETE", HCAPI.appointment + e.target.getAttribute("id"), true);
	xhr.send();
}

const restoreDelete = (e) => {
	e.target.innerText = "DELETE";
}

// Initialisation

var app = {};
var pros = [];
var apps = [];
var user = {};
var appointments = [];
var cookies = {};

// UI Binding

app.heroToUserLinks = ec("hero-to-user-link");
app.heroToProLinks = ec("hero-to-pro-link");

app.navItems = ec("nav-item");
app.frames = ec("frame");
app.proItems = ec("pro-item");
app.appItems = ec("app-item");

app.heroLoginBtn = eid("heroLoginBtn");
app.heroProList = eid("hero-pro-list");
app.proList = eid("proList");
app.appList = eid("appList");

app.userLogin = eid("user-login");
app.userProfile = eid("user-profile");
app.loginEmail = eid("login-email");
app.loginPassword = eid("login-password");
app.loginPhone = eid("login-phone");
app.loginName = eid("login-name");
app.loginAddress = eid("login-address");
app.loginNote = eid("login-note");
app.loginSubmitBtn = eid("login-submit-btn");
app.signupSubmitBtn = eid("signup-submit-btn");

app.userName = eid("user-name");
app.userNameTable = eid("user-name-table");
app.userAddress = eid("user-address");
app.userPhone = eid("user-phone");
app.userEmail = eid("user-email");
app.profileNote = eid("profile-note");
app.userInfoEditBtn = eid("user-info-edit");
app.userLogoutBtn = eid("user-logout");

app.newProBtn = eid("new-pro-btn");
app.newAppBtn = eid("new-app-btn");

app.newProPanel = eid("new-pro-panel");
app.newProName = eid("new-pro-name");
app.newProType = eid("new-pro-type");
app.newProRate = eid("new-pro-rate");
app.newProEmail = eid("new-pro-email");
app.newProNote = eid("new-pro-note");
app.proInfoSubmitBtn = eid("pro-info-submit");

app.newAppPanel = eid("new-app-panel");
app.newAppPro = eid("new-app-pro");
app.newAppY = eid("new-app-y");
app.newAppM = eid("new-app-m");
app.newAppD = eid("new-app-d");
app.newAppH = eid("new-app-h");
app.newAppDuration = eid("new-app-duration");
app.newAppMsg = eid("new-app-msg");
app.newAppNote = eid("new-app-note");
app.newAppSubmitBtn = eid("new-app-submit");

// Event Handlers

for (var i = 0; i < app.navItems.length; i++) {
	app.navItems[i].addEventListener("click", navActivate);
}

for (var i = 0; i < app.heroToProLinks.length; i++) {
	app.heroToProLinks[i].addEventListener("click", () => {
		eid("navProfessional").click();
	});
}

for (var i = 0; i < app.heroToUserLinks.length; i++) {
	app.heroToUserLinks[i].addEventListener("click", () => {
		eid("navUser").click();
	});
}

app.loginSubmitBtn.addEventListener("click", doLogin);
app.signupSubmitBtn.addEventListener("click", doSignup);
app.userInfoEditBtn.addEventListener("click", doUpdateInfo);
app.userLogoutBtn.addEventListener("click", doLogout);
app.newProBtn.addEventListener("click", () => {app.newProPanel.classList.remove("hidden")});
app.proInfoSubmitBtn.addEventListener("click", addNewPro);

app.newAppY.addEventListener("change", YChanged);
app.newAppM.addEventListener("change", MChanged);
app.newAppD.addEventListener("change", DChanged);
app.newAppBtn.addEventListener("click", () => {app.newAppPanel.classList.remove("hidden")});
app.newAppSubmitBtn.addEventListener("click", addNewApp);

// UI Data Init

getUserProfile();
getAllPros();

var getAllAppsInterval = setInterval(() => {
	if (pros.length != 0) {
		getAllApps();
		clearInterval(getAllAppsInterval);
	}
}, 200);